//
//  PACanadaFactsTests.m
//  PACanadaFactsTests
//
//  Created by Razvan Balazs on 3/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "PAFact.h"

@interface PACanadaFactsTests : XCTestCase

@end

@implementation PACanadaFactsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // Construct a dict.
    NSDictionary *testDict = @{@"title":@"Flag",
                               @"description": [NSNull null],
                               @"imageHref": @"http://images.findicons.com/files/icons/662/world_flag/128/flag_of_canada.png"
                               };

    
    PAFact *fact = [PAFact factFromDictionary:testDict];
    XCTAssertEqual(fact.title, @"Flag");
    XCTAssertEqual(fact.factDescription, nil);
    XCTAssertEqual(fact.imageHref, @"http://images.findicons.com/files/icons/662/world_flag/128/flag_of_canada.png");

}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
