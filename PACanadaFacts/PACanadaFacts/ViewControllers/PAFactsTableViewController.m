//
//  PAFactsTableViewController.m
//  PACanadaFacts
//
//  Created by Razvan Balazs on 3/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import "PAFactsTableViewController.h"
#import "PAFactsFetcherService.h"
#import "PAFact.h"
#import "PAFactCell.h"

static NSString *kCellIdentifier = @"kCellId";
static NSString *kCellImageIdentifier = @"kCellImageId";

@interface PAFactsTableViewController()

@property (nonatomic, strong) NSArray *factList;
@end

@implementation PAFactsTableViewController {
    UIActivityIndicatorView *_loadingIndicator; // Used as the background of the table view.
    NSMutableDictionary *_imagesDict; // keep the images after they are downloaded.
    PAFactCell *_sizingCell;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerClass:[PAFactCell class] forCellReuseIdentifier:kCellIdentifier];
    [self.tableView registerClass:[PAFactCell class] forCellReuseIdentifier:kCellImageIdentifier];

    self.factList = [[NSArray alloc] init];
    _imagesDict = [[NSMutableDictionary alloc] init]; // manually released.
    
    [self showLoading];
    [PAFactsFetcherService requestFactListWithCompletionBlock:^(NSString *title, NSArray *itemList) {
        // On main thread here, no need for weak.
        [self hideLoading];
        if (itemList) {
            self.factList = [NSArray arrayWithArray:itemList];
            [self.tableView reloadData];
            self.title = title;
        } else {
            // TODO display erorr.
        }

    }];
}

- (void)dealloc {
    [_factList release];
    [_imagesDict release];
    [_sizingCell release];
    [super dealloc];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PAFact *fact = [_factList objectAtIndex:indexPath.row];
 
    if (!_sizingCell) {
        _sizingCell = [[PAFactCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kCellIdentifier];
    }

    // configure the cell
    [_sizingCell updateWithTitle:fact.title andDescription:fact.factDescription hasMedia:fact.imageHref ? YES :NO];
    _sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_sizingCell.bounds));

    // force layout
    [_sizingCell setNeedsLayout];
    [_sizingCell layoutIfNeeded];
    
    CGFloat labelsHeight = [_sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height +1;
    return labelsHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _factList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PAFactCell *cell;
    PAFact *fact = [_factList objectAtIndex:indexPath.row];
    if (fact.imageHref) {
        cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:kCellImageIdentifier forIndexPath:indexPath];
    }
    [cell updateWithTitle:fact.title andDescription:fact.factDescription hasMedia:fact.imageHref ? YES:NO];
    
    // Handle images lazily. Store the downloaded ones in a dictionary.
    cell.tag = indexPath.row;
    if (_imagesDict[fact.imageHref]) {
        [cell updateImage:_imagesDict[fact.imageHref]];
    } else {
        [cell updateImage:[UIImage imageNamed:@"placeholder"]];
        if (fact.imageHref != nil) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:fact.imageHref]]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (image && (cell.tag == indexPath.row)) {
                        _imagesDict[fact.imageHref] = image;
                        [cell updateImage:_imagesDict[fact.imageHref]];
                    }
                });
            });
        }
    }
    return cell;
}

#pragma mark - LoadingIndicator

/// Present loading spinner.
- (void)showLoading {
    if (!_loadingIndicator) {
        _loadingIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        self.tableView.backgroundView = _loadingIndicator;
    }
    [_loadingIndicator startAnimating];
}

/// Hide the loading spinner.
- (void)hideLoading {
    [_loadingIndicator stopAnimating];
}

@end
