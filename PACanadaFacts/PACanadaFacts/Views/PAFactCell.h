//
//  PAFactCell.h
//  PACanadaFacts
//
//  Created by Razvan Balazs on 9/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// This cell represents the graphic representation of a fact object.

#import <UIKit/UIKit.h>
extern const CGFloat kCellGeneralPadding;

@interface PAFactCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *factDetailsLabel;
@property (nonatomic, strong) UIImageView *factImageView;

/// Update the cell with info.
- (void)updateWithTitle:(NSString *)title andDescription:(NSString *)description hasMedia:(BOOL)hasMedia;

/// Update with the new image after it is received.
- (void)updateImage:(UIImage *)image;

@end
