//
//  PAFactCell.m
//  PACanadaFacts
//
//  Created by Razvan Balazs on 9/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// This cell represents the graphic representation of a fact object.

#import "PAFactCell.h"
const CGFloat kCellGeneralPadding = 8.;
static const CGFloat kImageCellWidth = 70.;

@implementation PAFactCell {
    BOOL _hasMedia;
    NSMutableArray *_imageViewConstraints;
    NSMutableArray *_rightSideConstraints;

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] ) {

        self.titleLabel = [[[UILabel alloc] init] autorelease];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        self.titleLabel.textColor = [UIColor blueColor];
        [self.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.titleLabel];
        
        self.factDetailsLabel = [[[UILabel alloc] init] autorelease];
        self.factDetailsLabel.numberOfLines = 0;
        [self.factDetailsLabel setLineBreakMode:NSLineBreakByWordWrapping];
        self.factDetailsLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.factDetailsLabel];

        self.factImageView = [[[UIImageView alloc] init] autorelease];
        self.factImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.factImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:self.factImageView];

        _imageViewConstraints = [[NSMutableArray alloc] init];
        _rightSideConstraints = [[NSMutableArray alloc] init];
        _hasMedia = NO;
        [self setupConstraints];
    }
    return self;
}

- (void)dealloc {
    _titleLabel = nil;
    _factDetailsLabel = nil;
    _factImageView = nil;
    [_imageViewConstraints release];
    [_rightSideConstraints release];
    
    [super dealloc];
}

- (void)updateWithTitle:(NSString *)title andDescription:(NSString *)description hasMedia:(BOOL)hasMedia {
    self.titleLabel.text = title;
    self.factDetailsLabel.text = description;
    _hasMedia = hasMedia;
    if (_hasMedia) {
        self.factImageView.hidden = NO;
    } else {
        self.factImageView.hidden = YES;
    }
    
    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
}

- (void)updateImage:(UIImage *)image {
    self.factImageView.image = image;
    
    [self setNeedsUpdateConstraints];
    [self setNeedsLayout];
}

#pragma mark - Constraints

- (CGFloat)rightPaddingForDetailedText {
    return _hasMedia ? 2 * kCellGeneralPadding + kImageCellWidth : kCellGeneralPadding;
}

- (void)updateConstraints {

    // update the right paddings
    for (NSLayoutConstraint *c in _rightSideConstraints) {
        c.constant = [self rightPaddingForDetailedText];
    }

    // add contraints if imageview is visible, otherwise remove them
    if (_hasMedia) {
        [self.contentView addConstraints:_imageViewConstraints];
    } else {
        [self.contentView removeConstraints:_imageViewConstraints];
    }
    
    [super updateConstraints];
}

- (void)setupConstraints {
    [self.contentView removeConstraints:self.contentView.constraints];

    NSDictionary *views = @{ @"contentView" : self.contentView,
                             @"titleLabel" : self.titleLabel,
                             @"factDetailsLabel" : self.factDetailsLabel,
                             @"factImageView" : self.factImageView};
    NSDictionary *metrics = @{ @"padding":@(kCellGeneralPadding)};

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[titleLabel]-padding-|"
                                                                             options:0
                                                                             metrics:metrics
                                                                               views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[factDetailsLabel]"
                                                                             options:0
                                                                             metrics:metrics
                                                                               views:views]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-padding-[titleLabel]-padding-[factDetailsLabel]->=padding-|"
                                                                             options:NSLayoutFormatAlignAllLeft
                                                                             metrics:metrics
                                                                               views:views]];
    // constraints that change constants in the presence of imageview
    [_rightSideConstraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                                  attribute:NSLayoutAttributeRight
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.factDetailsLabel
                                                                  attribute:NSLayoutAttributeRight
                                                                 multiplier:1
                                                                   constant:[self rightPaddingForDetailedText]]];
    [self.contentView addConstraints:_rightSideConstraints];

    // imageview
    [_imageViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.factImageView
                                                                  attribute:NSLayoutAttributeTop
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self.titleLabel
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1
                                                                  constant:kCellGeneralPadding]];
    
    [_imageViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.contentView
                                                                      attribute:NSLayoutAttributeRight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:self.factImageView
                                                                      attribute:NSLayoutAttributeRight
                                                                     multiplier:1
                                                                       constant:kCellGeneralPadding]];

    NSLayoutConstraint *imageHeightConstraint = [NSLayoutConstraint constraintWithItem:self.contentView
                                                                   attribute:NSLayoutAttributeBottom
                                                                   relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                      toItem:self.factImageView
                                                                   attribute:NSLayoutAttributeBottom
                                                                  multiplier:1
                                                                    constant:kCellGeneralPadding];
    imageHeightConstraint.priority = UILayoutPriorityDefaultHigh;
    [_imageViewConstraints addObject:imageHeightConstraint];

    
    [_imageViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.factImageView
                                                                      attribute:NSLayoutAttributeWidth
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:kImageCellWidth]];
    
    [_imageViewConstraints addObject:[NSLayoutConstraint constraintWithItem:self.factImageView
                                                                      attribute:NSLayoutAttributeHeight
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:nil
                                                                      attribute:NSLayoutAttributeNotAnAttribute
                                                                     multiplier:1
                                                                       constant:kImageCellWidth]];

    [self.titleLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];
    [self.factDetailsLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisVertical];

}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    self.titleLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleLabel.frame);
    self.factDetailsLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.factDetailsLabel.frame);
}
@end
