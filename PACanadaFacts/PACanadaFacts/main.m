//
//  main.m
//  PACanadaFacts
//
//  Created by Razvan Balazs on 3/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
