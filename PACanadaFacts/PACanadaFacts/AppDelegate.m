//
//  AppDelegate.m
//  PACanadaFacts
//
//  Created by Razvan Balazs on 3/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//

#import "AppDelegate.h"
#import "PAFactsTableViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    PAFactsTableViewController *factsViewController = [[[PAFactsTableViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
    UINavigationController *rootViewControler = [[[UINavigationController alloc] initWithRootViewController:factsViewController] autorelease];
    rootViewControler.view.backgroundColor = [UIColor whiteColor];
    [self.window setRootViewController:rootViewControler];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
