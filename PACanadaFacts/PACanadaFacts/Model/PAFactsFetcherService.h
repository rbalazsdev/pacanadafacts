//
//  PAFactsFetcherService.h
//  PACanadaFacts
//
//  Created by Razvan Balazs on 9/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Download and initialize the Fact objects from the retrieved list.

#import <Foundation/Foundation.h>
#import "PAFact.h"

typedef void (^FetchServiceCompletionBlock) (NSString *title, NSArray *itemList);

@interface PAFactsFetcherService : NSObject

/// Request a list of facts. Completion block will be called with nil if there was an error.
+ (void)requestFactListWithCompletionBlock:(FetchServiceCompletionBlock)completionBlock;

@end
