//
//  PAFact.h
//  PACanadaFacts
//
//  Created by Razvan Balazs on 9/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// This object will represent a fact from the list.

#import <Foundation/Foundation.h>

@interface PAFact : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *factDescription; // description is reserved.
@property (nonatomic, copy) NSString *imageHref;

/// Init a PAFact from a json dictionary.
+ (instancetype)factFromDictionary:(NSDictionary *)dict;

@end
