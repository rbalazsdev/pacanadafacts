//
//  PAFact.m
//  PACanadaFacts
//
//  Created by Razvan Balazs on 9/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// This object will represent a fact from the list.

#import "PAFact.h"

@implementation PAFact

+ (instancetype)factFromDictionary:(NSDictionary *)dict {
    PAFact *fact = [[PAFact alloc] init];
    fact.title = dict[@"title"] != [NSNull null] ? dict[@"title"] : nil;
    fact.factDescription = dict[@"description"] != [NSNull null] ? dict[@"description"] : nil;
    fact.imageHref = dict[@"imageHref"] != [NSNull null] ? dict[@"imageHref"] : nil;
    if (!fact.title && !fact.factDescription && !fact.imageHref) {
        [fact release];
        return nil;
    }
    return [fact autorelease];
}

- (void)dealloc {
    _title = nil;
    _factDescription = nil;
    _imageHref = nil;
    [super dealloc];
}
@end
