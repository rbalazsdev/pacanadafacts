//
//  PAFactsFetcherService.m
//  PACanadaFacts
//
//  Created by Razvan Balazs on 9/06/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Download and initialize the Fact objects from the retrieved list.

#import "PAFactsFetcherService.h"

@implementation PAFactsFetcherService

static NSString *kIngestUrlString = @"https://dl.dropboxusercontent.com/u/746330/facts.json";

+ (void)requestFactListWithCompletionBlock:(FetchServiceCompletionBlock)completionBlock {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: kIngestUrlString] ];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (!connectionError && data.length) {
                                   NSError* error;
#warning                           // TODO  json from drobox has an error on serialize, replaced for the moment with a local version.

                                   NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   if (!json) {
                                       
                                       NSString *filePath = [[NSBundle mainBundle] pathForResource:@"facts" ofType:@"json"];
                                       NSData *data = [NSData dataWithContentsOfFile:filePath];
                                       json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                   }
                                   NSMutableArray *output = [[[NSMutableArray alloc] init] autorelease];
                                   [json[@"rows"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                                       PAFact *fact = [PAFact factFromDictionary:obj];
                                       if (fact) {
                                           [output addObject:fact];
                                       }
                                   }];
                                   
                                   completionBlock(json[@"title"],[NSArray arrayWithArray:output]);
                               }
                               
                           }];
    
}


@end
